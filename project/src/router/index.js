import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'users',
    component: () => import('../views/Company')
  },
  {
    path: '/company',
    name: 'users',
    component: () => import('../views/Company')
  },
  {
    path: '/postJob',
    name: 'postJob',
    component: () => import('../views/PostJob')
  },
  {
    path: '/joblists',
    name: 'joblists',
    component: () => import('../views/JobLists')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
